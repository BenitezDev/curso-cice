﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuberiasScript : MonoBehaviour {

    [SerializeField] float speed = 5f;
    [SerializeField] int timeToDestroy = 5;

    void Start ()
    {
        Destroy(this.gameObject, timeToDestroy);

        float desviacion = Random.Range(-2f, 2f);

        transform.position = new Vector3(transform.position.x, transform.position.y + desviacion, transform.position.z);
    }
	
	void Update () {
        if (GestorJuego.instance.GetJugando() == true)
        {
            transform.Translate(Vector3.back * Time.deltaTime * speed);
        }
	}
}
