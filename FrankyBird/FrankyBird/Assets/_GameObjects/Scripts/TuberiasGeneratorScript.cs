﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuberiasGeneratorScript : MonoBehaviour
{

    [SerializeField] GameObject tuberiasPrefab;
    [SerializeField] float tiempoEntreTuberias = 2.2f;

	void Start () {
        InvokeRepeating("CrearTuberia", 0, tiempoEntreTuberias);
    }
    
    private void CrearTuberia() {
        if (GestorJuego.instance.GetJugando() == true) {
            Instantiate(tuberiasPrefab,transform);
        }
    }
}
