﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrankieScript : MonoBehaviour {

    [SerializeField] GameObject sangrePrefab;
    [SerializeField] private float force = 10f;
    [SerializeField] AudioClip aleteo;
    [SerializeField] AudioClip puntuacion;
 
    private Rigidbody rb;
    private AudioSource audioSource;

    bool flag = true;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

	}
	

	void Update ()
    {
        
        if (Input.GetKeyDown(KeyCode.Space)) {
            Impulsar();
        }
        
        if(rb.velocity.y > 0)
        {
            Quaternion rot1 = Quaternion.Euler(new Vector3(-25, 0, 0));
            transform.rotation = Quaternion.Lerp(transform.rotation,  rot1 , Time.deltaTime*20);
        }
        else
        {
            Quaternion rot2 = Quaternion.Euler(new Vector3(25, 0, 0));
            transform.rotation = Quaternion.Lerp(transform.rotation, rot2, Time.deltaTime*10);   
        }
        

        //transform.rotation = Quaternion.Euler(new Vector3(rb.velocity.y*-10f, 0, 0));


    }

    void Impulsar() 
    {
        print("impulsar");
        flag = false;
        rb.AddForce(Vector3.up * force, ForceMode.Impulse);
        audioSource.PlayOneShot(aleteo);
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Tuberia") )
        {
            GestorJuego.instance.SetJugando(false);
            GameObject sangre = Instantiate(sangrePrefab);
            sangre.transform.position = this.transform.position;
            GestorJuego.instance.FinalizarPartida();
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Point")) {
            GestorJuego.instance.puntos++;
            GestorJuego.instance.textPoints.text = "Puntos: " + GestorJuego.instance.puntos;
        }
    }

}
