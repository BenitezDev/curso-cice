﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GestorJuego : MonoBehaviour
{
    public static GestorJuego instance = null;

    [SerializeField] GameObject panelmenu;

    private void Awake() {
        if (instance == null)
            instance = this;

        else
            if(instance!=this)
            Destroy(gameObject);
    }

    private void Start() {
        panelmenu.SetActive(false);
    }

    public Text textPoints;

    public int puntos = 0;
    private bool jugando = true;

    public bool GetJugando   ()         { return jugando; }
    public void SetJugando(bool value)  { jugando = value; }

    public void FinalizarPartida() {
        jugando = false;

        panelmenu.SetActive(true);
   
    }

    private void RecargarEscena()
    {
        SceneManager.LoadScene(0);
    }


    public void botonPlay() {
        RecargarEscena();
    }

    public void botonSalir() 
    {
        Application.Quit();    
    }
}
